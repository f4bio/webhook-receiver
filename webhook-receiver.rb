require 'webrick'

server = WEBrick::HTTPServer.new(:Port => ARGV.first)
server.mount_proc '/' do |req, res|
  system('cd $HOME/instance-setup && docker-compose pull && docker-compose up -d')
end

trap 'INT' do
  server.shutdown
end
server.start
